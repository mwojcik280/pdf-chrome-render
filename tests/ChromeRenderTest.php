<?php

use PHPUnit\Framework\TestCase;

class ChromeRenderTest extends TestCase {

    public function testChromeRender()
    {
        $Renderer = new ChromeRender;
        $this->assertEquals($Renderer->exists(), true);
    }

    public function testChromeRenderAdd()
    {
        $Renderer = new ChromeRender;
        $this->assertEquals($Renderer->add(2,2), 4);
    }
}